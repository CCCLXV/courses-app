import React, { Suspense, lazy, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { initialAuth, getMyRole, logoutUser } from './store/reducers/user';
import Header from './components/Header';
import PrivateRoute from './components/PrivateRoute';
import './App.css';

const HomePage = lazy(() => import('./pages/Home'));
const CreateCourse = lazy(() => import('./pages/CreateCourse'));
const UpdateCourse = lazy(() => import('./pages/UpdateCourse'));
const LoginPage = lazy(() => import('./pages/Login'));
const SignupPage = lazy(() => import('./pages/Signup'));
const SingleCourse = lazy(() => import('./pages/SingleCourse'));

function App() {
    const dispatch = useDispatch();
    const history = useHistory();
    useEffect(() => {
        dispatch(initialAuth());
        dispatch(getMyRole());
    }, [dispatch]);
    const userInfo = useSelector(state => state.user);
    const handleLogout = () => {
        dispatch(logoutUser());
        history.push('/');
    };
    const userAuth = useSelector(state => state.user);
    return (
        <div className="App" data-auto="app">
            <Router>
                <Header name={userAuth.name} token={userAuth.token} userInfo={userInfo} handleLogout={handleLogout} />
                <Suspense fallback={<h1>Loading...</h1>}>
                    <>
                        {userAuth.isAuth ? (
                            <Switch>
                                <Route exact path="/" component={HomePage} />
                                <PrivateRoute path="/courses/add" component={CreateCourse} />
                                <PrivateRoute path="/courses/update/:id" component={UpdateCourse} />
                                <Route path="/courses/:id" component={SingleCourse} />
                            </Switch>
                        ) : (
                            <Switch>
                                <Route exact path="/" render={() => <LoginPage />} />
                                <Route path="/sign-up" component={SignupPage} />
                            </Switch>
                        )}
                    </>
                </Suspense>
            </Router>
        </div>
    );
}

export default App;
