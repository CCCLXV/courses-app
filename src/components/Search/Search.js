import React from 'react';

const Search = () => {
    return (
        <div className="d-flex align-items-center">
            <input type="text" className="form-control me-3" placeholder="e.g. JavaScript" />
            <button className="btn btn-primary">Search</button>
        </div>
    );
};

export default Search;
