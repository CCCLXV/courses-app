import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const Header = ({ name, handleLogout, userInfo = { isAuth: false } }) => {
    return (
        <nav className="navbar navbar-light bg-light" data-testid="header-wrapper">
            <div className="container d-flex justify-content-between">
                <Link className="navbar-brand" to="/">
                    <img
                        src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg"
                        alt=""
                        width="30"
                        height="24"
                        className="d-inline-block align-text-top"
                        data-tesid="header-logo"
                        id="header-logo"
                    />
                    Bootstrap
                </Link>
                {userInfo.isAuth && (
                    <div className="d-flex align-items-center">
                        <h4 className="mx-3" id="header-user-name" data-testid="header-user-name">
                            {name}
                        </h4>
                        <button
                            className="btn btn-primary"
                            onClick={handleLogout}
                            data-testid="logout-button"
                            id="logout-button"
                        >
                            Logout
                        </button>
                    </div>
                )}
            </div>
        </nav>
    );
};

Header.propTypes = {
    name: PropTypes.string
};

Header.defaultProps = {
    name: 'name'
};

export default Header;
