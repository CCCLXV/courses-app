import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './Header';

const defaultProps = {
    name: 'Khumoyun Inoyatov',
    handleLogout: () => console.log('logged out'),
    userInfo: {
        isAuth: true
    }
};

describe('Header component', () => {
    const component = render(
        <Router>
            <Header {...defaultProps} />
        </Router>
    ).getByTestId('header-wrapper');
    test('Should render header component', () => {
        expect(component).toBeInTheDocument();
    });
    test('Header should have logo', () => {
        const logo = component.querySelector('#header-logo');
        console.log(logo);
        expect(logo).toHaveAttribute('src', 'https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg');
    });
    test('Header should have button', () => {
        const button = component.querySelector('#logout-button');
        expect(button).toHaveTextContent('Logout');
    });
    test('Header should have name', () => {
        const name = component.querySelector('#header-user-name');
        expect(name).toHaveTextContent('Khumoyun Inoyatov');
    });
});
