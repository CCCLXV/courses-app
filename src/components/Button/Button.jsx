import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ viewType, content, onClick, ...rest }) => {
    return (
        <button className={`btn btn-${viewType}`} onClick={onClick} {...rest}>
            {content}
        </button>
    );
};

Button.propTypes = {
    viewType: PropTypes.string,
    content: PropTypes.string,
    onClick: PropTypes.func
};

Button.defaultProps = {
    viewType: 'primary',
    content: '',
    onClick: () => {}
};

export default Button;
