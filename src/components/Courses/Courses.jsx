import React from 'react';
import CoursesCard from '../CourseCard';

const Courses = ({ isLoading, items }) => {
    return (
        <div data-testid="courses" id="courses">
            {isLoading ? (
                items.map(item => (
                    <CoursesCard
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        description={item.description}
                        creationDate={item.creationDate}
                        duration={item.duration}
                        authors={item.authors}
                    />
                ))
            ) : (
                <div className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            )}
        </div>
    );
};

export default Courses;
