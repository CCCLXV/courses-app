import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../../store';
import Courses from './Courses';

const defaultProps = {
    isLoading: true,
    items: [
        {
            id: '1',
            title: 'Test 1',
            description: 'Desc 1',
            creationDate: '11/12/1997',
            duration: 111,
            authors: ['asdf-bsda-qwer-qswr']
        },
        {
            id: '2',
            title: 'Test 2',
            description: 'Desc 2',
            creationDate: '11/12/1997',
            duration: 111,
            authors: ['asdf-bsda-qwer-qswr']
        }
    ]
};

describe('CourseCard component', () => {
    const component = render(
        <Provider store={store}>
            <Router>
                <Courses {...defaultProps} />
            </Router>
        </Provider>
    ).getByTestId('courses');
    test('CourseCard should be in document', () => {
        expect(component).toBeInTheDocument();
    });
    test('should display amount of CourseCard equal to length of array', () => {
        expect(component.childNodes.length).toEqual(defaultProps.items.length);
    });
    test('should display empty container if array is empty', () => {
        const component = render(<Courses isLoading items={[]} />).getByTestId('courses');
        expect(component.childNodes.length).toEqual(0);
    });
});
