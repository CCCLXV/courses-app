import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = props => {
    const userRole = useSelector(state => state.user.role);
    return <>{userRole === 'admin' ? <Route {...props} /> : <Redirect to="/" />}</>;
};

export default PrivateRoute;
