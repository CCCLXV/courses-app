import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import CoursesCard from './CoursesCard';
import store from '../../store';

const defaultProps = {
    id: '1',
    title: 'Test title',
    description: 'Test description',
    creationDate: '11/12/1997',
    duration: 111,
    authors: ['Khumoyun Inoyatov']
};

describe('CourseCard component', () => {
    const component = render(
        <Provider store={store}>
            <Router>
                <CoursesCard {...defaultProps} />
            </Router>
        </Provider>
    ).getByTestId('course-card');
    test('CourseCard should be in document', () => {
        expect(component).toBeInTheDocument();
    });

    test('CourseCard should display title', () => {
        const title = component.querySelector('#course-card-title');
        expect(title).toHaveTextContent(defaultProps.title);
    });

    test('CourseCard should display duration', () => {
        const title = component.querySelector('#course-card-duration');
        expect(title).toHaveTextContent(
            `Duration: ${Math.floor(defaultProps.duration / 60)}:${defaultProps.duration % 60} hours`
        );
    });

    test('CourseCard should display authors list', () => {
        const title = component.querySelector('#course-card-authors-list');
        expect(title).toHaveTextContent(`Authors:`);
    });

    test('CourseCard should display created date', () => {
        const title = component.querySelector('#course-card-date');
        expect(title).toHaveTextContent(`Created:`);
    });
});
