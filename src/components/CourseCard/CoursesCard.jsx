import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { removeCourse } from '../../store/reducers/courses';
import { usedAuthorsList } from '../../helpers/usedAuthorsList';
import './CoursesCard.scss';

const CoursesCard = ({ id, title, description, creationDate, duration, authors }) => {
    const dispatch = useDispatch();
    const fullAuthorsList = useSelector(state => state.authors.list);
    const userRole = useSelector(state => state.user.role);

    return (
        <div className="card mb-3 course-card" data-testid="course-card">
            <div className="card-body d-flex align-items-center justify-content-between">
                <div className="card-left">
                    <h1 className="text-start" data-testid="course-card-title" id="course-card-title">
                        {title}
                    </h1>
                    <p className="text-start">{description}</p>
                </div>
                <div className="card-right">
                    <p className="text-start" id="course-card-authors-list">
                        <strong>Authors:</strong>
                        {usedAuthorsList(authors, fullAuthorsList)
                            .map(item => {
                                return item.name;
                            })
                            .toString()}
                    </p>
                    <p className="text-start" id="course-card-duration">
                        <strong>Duration:</strong> {Math.floor(duration / 60)}:{duration % 60} hours
                    </p>
                    <p className="text-start" id="course-card-date">
                        <strong>Created:</strong>
                        {creationDate}
                    </p>
                    <Link to={`/courses/${id}`} className="btn btn-primary me-3">
                        Show course
                    </Link>
                    {userRole === 'admin' && (
                        <>
                            <button className="btn btn-danger me-3" onClick={() => dispatch(removeCourse(id))}>
                                Remove
                            </button>
                            <Link to={`/courses/update/${id}`} className="btn btn-success">
                                Edit
                            </Link>
                        </>
                    )}
                </div>
            </div>
        </div>
    );
};

CoursesCard.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    creationDate: PropTypes.string,
    duration: PropTypes.number,
    authors: PropTypes.arrayOf(PropTypes.string)
};

CoursesCard.defaultProps = {
    id: 0,
    title: 'title',
    description: 'desc',
    creationDate: '',
    duration: '',
    authors: []
};

export default CoursesCard;
