import React, { useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAuthors, createAuthor } from '../../store/reducers/authors';
import { usedAuthorsList } from '../../helpers/usedAuthorsList';

const CourseForm = ({ handler, defaultValues, update }) => {
    const { register, handleSubmit } = useForm();
    const dispatch = useDispatch();
    const { list, isLoading } = useSelector(state => state.authors);
    const [duration, setDuration] = useState(0);
    const [selectedAuthors, setSelectedAuthors] = useState([]);
    const newAuthorRef = useRef(null);
    const timeInputRef = useRef(null);
    const titleInputRef = useRef(null);
    const descInputRef = useRef(null);

    useEffect(() => {
        dispatch(fetchAuthors());
        if (update) {
            setSelectedAuthors(usedAuthorsList(defaultValues.authors, list));
        }
    }, [dispatch]);

    const addAuthor = (e, author) => {
        setSelectedAuthors(oldSelectedAuthors => {
            return [...oldSelectedAuthors, author];
        });
        e.preventDefault();
    };

    const removeAuthor = id => {
        setSelectedAuthors(oldSelectedAuthors => {
            return oldSelectedAuthors.filter(author => author.id !== id);
        });
    };
    const addNewAuthor = e => {
        e.preventDefault();
        dispatch(
            createAuthor({
                formData: {
                    name: newAuthorRef.current.value
                }
            })
        );
    };
    return (
        <form onSubmit={handleSubmit(e => handler(e, selectedAuthors, timeInputRef.current.value))}>
            <div className="d-flex align-items-center justify-content-between">
                <div className="mb-3">
                    <label htmlFor="exampleFormControlInput1" className="form-label text-align-left">
                        Title
                    </label>
                    <input
                        type="text"
                        name="title"
                        required
                        className="form-control"
                        defaultValue={defaultValues?.title}
                        id="exampleFormControlInput1"
                        placeholder="Enter title..."
                        ref={titleInputRef}
                        {...register('title')}
                    />
                </div>
                <button type="submit" className="btn btn-success">
                    {update ? 'Update course' : 'Create course'}
                </button>
            </div>
            <div className="mb-5">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">
                    Description
                </label>
                <textarea
                    name="description"
                    className="form-control"
                    id="exampleFormControlTextarea1"
                    defaultValue={defaultValues?.description}
                    rows="3"
                    placeholder="Enter description"
                    ref={descInputRef}
                    {...register('description')}
                />
            </div>
            <fieldset>
                <div className="row mb-5">
                    <div className="col-lg-6">
                        <h2 className="mb-3">Add author</h2>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">
                                Title
                            </label>
                            <input
                                type="text"
                                name="title"
                                className="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Enter title..."
                                ref={newAuthorRef}
                            />
                        </div>
                        <button onClick={addNewAuthor} className="btn btn-primary">
                            Create Author
                        </button>
                    </div>
                    <div className="offset-lg-3 col-lg-3">
                        <h2 className="mb-3">Authors</h2>
                        {isLoading &&
                            list.map(author => (
                                <div className="d-flex justify-content-between mb-3" key={author.id}>
                                    <span>{author.name}</span>
                                    <button className="btn btn-secondary" onClick={e => addAuthor(e, author)}>
                                        Add author
                                    </button>
                                </div>
                            ))}
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <h2 className="mb-3">Duration</h2>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">
                                Duration
                            </label>
                            <input
                                type="number"
                                name="duration"
                                required
                                defaultValue={defaultValues?.duration}
                                className="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Enter duration in minutes"
                                ref={timeInputRef}
                                onChange={() => setDuration(timeInputRef.current.value)}
                            />
                        </div>

                        <h2>
                            Duration: {duration > 0 ? `${Math.floor(duration / 60)}:${duration % 60}` : '00:00'} hours
                        </h2>
                    </div>

                    <div className="offset-lg-3 col-lg-3">
                        <h2 className="mb-3">Course authors</h2>
                        <div className="mb-3">
                            {selectedAuthors.length > 0 ? (
                                selectedAuthors.map(item => (
                                    <div key={item.id} className="d-flex justify-content-between mb-2">
                                        <span className="me-3">{item.name}</span>
                                        <button className="btn btn-danger" onClick={() => removeAuthor(item.id)}>
                                            X
                                        </button>
                                    </div>
                                ))
                            ) : (
                                <span>Author list is empty</span>
                            )}
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    );
};

export default CourseForm;
