import { pipeDuration } from './pipeDuration';

describe('Should test pipeDuration', () => {
    test('Should return time in HH:MM format', () => {
        expect(pipeDuration(200)).toEqual('3:20');
    });
});
