export const pipeDuration = timeInMinutes => {
    return `${Math.floor(timeInMinutes / 60)}:${timeInMinutes % 60}`;
};
