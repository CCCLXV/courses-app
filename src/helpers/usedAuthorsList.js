export const usedAuthorsList = (authors, fullAuthorsList) => {
    const list = [];
    authors.forEach(author => {
        fullAuthorsList.forEach(anAuthor => {
            if (author === anAuthor.id) {
                list.push(anAuthor);
            }
        });
    });
    return list;
};
