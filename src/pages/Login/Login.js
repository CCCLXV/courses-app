import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { authUser } from '../../store/reducers/user';

const Login = () => {
    const dispatch = useDispatch();
    const { hasError, message } = useSelector(state => state.user);
    const { register, handleSubmit } = useForm();
    const handleLogin = data => {
        dispatch(authUser(data));
    };
    return (
        <div className="container">
            <div className="row">
                <div className="offset-lg-3 col-lg-6 mt-5">
                    <div className="card">
                        <div className="card-header">
                            <h3>Login</h3>
                        </div>
                        <form onSubmit={handleSubmit(handleLogin)} className="p-3 text-start">
                            {hasError && <div className="alert alert-danger">{message}</div>}
                            <div className="mb-3">
                                <label htmlFor="email-input" className="form-label">
                                    Email
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="email-input"
                                    placeholder="Enter email"
                                    name="email"
                                    {...register('email')}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password-input" className="form-label">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password-input"
                                    placeholder="Enter password"
                                    name="password"
                                    {...register('password')}
                                />
                            </div>
                            <button type="submit" className="btn btn-primary mb-3">
                                Login
                            </button>
                            <p>
                                If you not have an account you can <Link to="/sign-up">Registration</Link>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
