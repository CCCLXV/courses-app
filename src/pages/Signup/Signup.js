import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import http from '../../services/http';

const Signup = () => {
    const history = useHistory();
    const { register, handleSubmit } = useForm();
    const [formInfo, setFormInfo] = useState({
        hasError: false,
        isSucceed: false,
        errors: []
    });
    const handleLogin = values => {
        http.request
            .post('/register', values)
            .then(() => {
                setFormInfo({
                    hasError: false,
                    isSucceed: true
                });
                setTimeout(() => {
                    history.push('/');
                }, 2000);
            })
            .catch(err => {
                setFormInfo({
                    hasError: true,
                    isSucceed: false,
                    errors: err.response.data.errors
                });
            });
    };
    return (
        <div className="container">
            <div className="row">
                <div className="offset-lg-3 col-lg-6 mt-5">
                    <div className="card">
                        <div className="card-header">
                            <h3>Registration</h3>
                        </div>
                        <form onSubmit={handleSubmit(handleLogin)} className="p-3 text-start">
                            {formInfo.isSucceed && (
                                <div className="alert alert-success">Success! You'll be redirected.</div>
                            )}
                            {formInfo.hasError &&
                                formInfo.errors.map((item, index) => (
                                    <div key={index} className="alert alert-danger">
                                        {item}
                                    </div>
                                ))}
                            <div className="mb-3">
                                <label htmlFor="name-input" className="form-label">
                                    Name
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name-input"
                                    placeholder="Enter name"
                                    name="name"
                                    {...register('name')}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="email-input" className="form-label">
                                    Email
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="email-input"
                                    placeholder="Enter email"
                                    name="email"
                                    {...register('email')}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password-input" className="form-label">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password-input"
                                    placeholder="Enter password"
                                    name="password"
                                    {...register('password')}
                                />
                            </div>
                            <button type="submit" className="btn btn-primary mb-3">
                                Registration
                            </button>
                            <p>
                                If you have an account you can <Link to="/">Login</Link>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Signup;
