import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import CourseForm from '../../components/CourseForm';
import { updateCourse, getSingleCourse } from '../../store/reducers/courses';

const UpdateCourse = ({ match }) => {
    const history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getSingleCourse(match.params.id));
    }, [match.params.id]);
    const singleCourse = useSelector(state => state.courses.single);
    const createCourse = (values, selectedAuthors, duration) => {
        dispatch(
            updateCourse({
                id: match.params.id,
                formData: {
                    title: values.title,
                    description: values.description,
                    // eslint-disable-next-line radix
                    duration: parseInt(duration),
                    authors: selectedAuthors.map(item => item.id)
                },
                history
            })
        );
    };

    return (
        <div className="container mt-3 text-start">
            {singleCourse.isFetched && <CourseForm handler={createCourse} defaultValues={singleCourse.data} update />}
        </div>
    );
};

export default UpdateCourse;
