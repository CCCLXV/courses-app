import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import CourseContainer from '../../containers/CourseContainer';
import Search from '../../components/Search';

const Home = () => {
    const userRole = useSelector(state => state.user.role);
    return (
        <div className="container">
            <div className="d-flex justify-content-between my-3">
                <Search />
                <Link to="/courses/add">
                    <button className="btn btn-primary" disabled={userRole !== 'admin'}>
                        Add new course
                    </button>
                </Link>
            </div>
            <CourseContainer />
        </div>
    );
};

export default Home;
