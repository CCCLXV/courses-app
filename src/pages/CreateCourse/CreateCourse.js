import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import CourseForm from '../../components/CourseForm';
import { createNewCourse } from '../../store/reducers/courses';

const CreateCourse = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const createCourse = (values, selectedAuthors, duration) => {
        dispatch(
            createNewCourse({
                formData: {
                    title: values.title,
                    description: values.description,
                    // eslint-disable-next-line radix
                    duration: parseInt(duration),
                    authors: selectedAuthors.map(item => item.id)
                },
                history
            })
        );
    };

    return (
        <div className="container mt-3 text-start">
            <CourseForm handler={createCourse} />
        </div>
    );
};

export default CreateCourse;
