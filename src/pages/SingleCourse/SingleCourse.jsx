import React, { useEffect, useState } from 'react';
import http from '../../services/http';

const SingleCourse = ({ match }) => {
    const [courses, setCourses] = useState({
        isFetched: false,
        data: []
    });
    useEffect(() => {
        http.request.get(`/courses/${match.params.id}`).then(res => {
            setCourses({
                isFetched: true,
                data: res.data.result
            });
        });
    }, []);
    return (
        <div className="container">
            {courses.isFetched && (
                <div className="mt-5">
                    <h1>{courses.data.title}</h1>
                    <div className="row">
                        <div className="col-lg-8">
                            <p>{courses.data.description}</p>
                        </div>
                        <div className="col-lg-4 text-start">
                            <h4>
                                <strong>ID:</strong> {courses.data.id}
                            </h4>
                            <h4>
                                <strong>Duration:</strong> {Math.floor(courses.data.duration / 60)}:
                                {courses.data.duration % 60} hours
                            </h4>
                            <h4>
                                <strong>Created:</strong> {courses.data.creationDate}
                            </h4>
                            <h4>
                                <strong>ID:</strong> {courses.data.id}
                            </h4>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default SingleCourse;
