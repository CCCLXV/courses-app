import coursesList from './coursesList';
import authorsList from './authorsList';

export { coursesList, authorsList };
