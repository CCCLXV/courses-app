import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import store from '../store';
import App from '../App';

describe('App component', () => {
    test('Connect Provider to App', () => {
        render(
            <Provider store={store}>
                <App />
            </Provider>
        );
    });
});
