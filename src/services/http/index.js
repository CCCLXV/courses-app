import axios from 'axios';

const request = axios.create({
    baseURL: 'http://localhost:3000',
    timeout: 1000
});

let token = window.localStorage.getItem('lData');

const subscribe = store => {
    const state = store.getState();

    if (state.user.token) {
        token = state.user.token;
    }
    if (token !== null) {
        request.defaults.headers['Authorization'] = token;
    }
};

export default {
    request,
    subscribe
};
