import { configureStore } from '@reduxjs/toolkit';
import userReducer from './reducers/user';
import authorsReducer from './reducers/authors';
import courses from './reducers/courses';

const store = configureStore({
    reducer: {
        user: userReducer,
        authors: authorsReducer,
        courses
    }
});

export default store;
