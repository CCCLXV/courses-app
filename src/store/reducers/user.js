import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import http from '../../services/http';

const initialState = {
    isAuth: false,
    name: '',
    email: '',
    token: null,
    role: '',
    hasError: false,
    message: 'Email or Password is incorrect!'
};

export const getMyRole = createAsyncThunk(
    'auth/getMe',
    async () =>
        (
            await http.request.get('/users/me', {
                headers: {
                    Authorization: localStorage.getItem('token')
                }
            })
        ).data
);

export const authUser = createAsyncThunk('auth/login', async (formData, { dispatch }) => {
    try {
        const result = (await http.request.post('/login', formData)).data;
        setTimeout(() => dispatch(getMyRole()), 0);
        return result;
    } catch (e) {
        console.error(e);
    }
});

export const initialAuth = createAsyncThunk('auth/refresh', async () => ({
    token: localStorage.getItem('token'),
    name: localStorage.getItem('name'),
    email: localStorage.getItem('email')
}));

export const logoutUser = createAsyncThunk('auth/logout', async () => (await http.request.delete('/logout')).data);

const slice = createSlice({
    name: 'users',
    initialState,
    reducers: {},
    extraReducers: {
        [authUser.fulfilled]: (state, action) => {
            state.token = action.payload.result;
            state.email = action.payload.user.email;
            state.name = action.payload.user.name;
            state.isAuth = true;
            localStorage.setItem('token', action.payload.result);
            localStorage.setItem('email', action.payload.user.email);
            localStorage.setItem('name', action.payload.user.name);
        },
        [authUser.rejected]: state => {
            state.isAuth = false;
            state.hasError = true;
        },
        [initialAuth.fulfilled]: (state, action) => {
            state.token = action.payload.token;
            state.email = action.payload.email;
            state.name = action.payload.name;
            if (action.payload.token) {
                state.isAuth = true;
            }
        },
        [logoutUser.fulfilled]: state => {
            state.token = null;
            state.email = '';
            state.name = '';
            state.isAuth = false;
            state.role = '';
            localStorage.removeItem('token');
            localStorage.removeItem('email');
            localStorage.removeItem('name');
        },
        [getMyRole.fulfilled]: (state, action) => {
            state.role = action.payload.result.role;
        }
    }
});

export default slice.reducer;
