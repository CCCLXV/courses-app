import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import http from '../../services/http';

const initialState = {
    list: [],
    isLoading: false,
    single: {
        data: null,
        isFetched: false
    }
};

const fetchCoursesList = createAsyncThunk(
    'courses/fetchList',
    async () => (await http.request.get('/courses/all')).data
);

const createNewCourse = createAsyncThunk(
    'courses/createCourse',
    async ({ formData, history }) =>
        (await http.request.post('/courses/add', formData).then(() => history.push('/'))).data
);

const removeCourse = createAsyncThunk('courses/removeCourse', async id => {
    return {
        id,
        data: (await http.request.delete(`/courses/${id}`)).data
    };
});

const getSingleCourse = createAsyncThunk(
    'courses/getSingleCourse',
    async id => (await http.request.get(`/courses/${id}`)).data
);

const updateCourse = createAsyncThunk(
    'courses/updateCourse',
    async ({ formData, id, history }) =>
        (await http.request.put(`/courses/${id}`, formData).then(() => history.push('/'))).data
);

const slice = createSlice({
    name: 'courses',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCoursesList.fulfilled]: (state, { payload }) => {
            state.list = payload.result;
            state.isLoading = true;
        },
        [fetchCoursesList.rejected]: state => {
            state.list = [];
        },
        [createNewCourse.fulfilled]: (state, action) => {
            state.list.push(action.payload.result);
            state.isLoading = true;
        },
        [removeCourse.fulfilled]: (state, action) => {
            state.list = state.list.filter(item => item.id !== action.payload.id);
        },
        [getSingleCourse.fulfilled]: (state, { payload }) => {
            state.single.data = payload.result;
            state.single.isFetched = true;
        },
        [updateCourse.fulfilled]: (state, action) => {
            state.list.push(action.payload.result);
            state.isLoading = true;
        }
    }
});

export { createNewCourse, fetchCoursesList, removeCourse, getSingleCourse, updateCourse };

export default slice.reducer;
