import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import http from '../../services/http';

const initialState = {
    list: [],
    isLoading: false
};

const fetchAuthors = createAsyncThunk(
    'courses/fetchAuthors',
    async () => (await http.request.get('/authors/all')).data
);

const createAuthor = createAsyncThunk(
    'courses/addAuthor',
    async ({ formData }) => (await http.request.post('/authors/add', formData)).data
);

const removeAuthor = createAsyncThunk('courses/removeAuthor', async id => {
    return {
        id,
        data: (await http.request.delete(`/authors/${id}`)).data
    };
});

const slice = createSlice({
    name: 'authors',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchAuthors.fulfilled]: (state, { payload }) => {
            state.list = payload.result;
            state.isLoading = true;
        },
        [fetchAuthors.rejected]: state => {
            state.list = [];
        },
        [createAuthor.fulfilled]: (state, action) => {
            state.list.push(action.payload.result);
            state.isLoading = true;
        },
        [removeAuthor.fulfilled]: (state, action) => {
            state.list = state.list.filter(item => item.id !== action.payload.id);
        }
    }
});

export { fetchAuthors, createAuthor, removeAuthor };

export default slice.reducer;
