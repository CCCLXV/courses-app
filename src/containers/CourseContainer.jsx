import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Courses from '../components/Courses';
import { fetchCoursesList } from '../store/reducers/courses';
import { fetchAuthors } from '../store/reducers/authors';

const CoursesContainer = () => {
    const dispatch = useDispatch();
    const { list, isLoading } = useSelector(state => state.courses);
    useEffect(() => {
        dispatch(fetchCoursesList());
        dispatch(fetchAuthors());
    }, [dispatch]);

    return <Courses isLoading={isLoading} items={list} />;
};

export default CoursesContainer;
